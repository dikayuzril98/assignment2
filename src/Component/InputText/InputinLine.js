import React, { Component } from 'react'
import { TextInput, StyleSheet, View,Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import  Icon  from 'react-native-vector-icons/FontAwesome'



 class InputinLine extends Component {
     constructor(props){
        super(props)   
        this.state = {
        isLook :false
        }
    }


 
    render() {
        const ShowPassword = () => {
            return(
                <TouchableOpacity onPress ={()=> this.setState({ isLook :!this.state.isLook})} style={{justifyContent:'center',flex:1}}>
                    <View>
                        {this.state.isLook ?
                        (<Icon name="eye" size ={25} />): 
                        (<Icon name='eye-slash' size={25} />
                         )}
                    </View>
                </TouchableOpacity>
        )
    }
        return (
            <View style= {styles.container}>
                <View><Text style={{fontWeight:'bold'}}>{this.props.form}</Text></View>
                
                <View style={styles.inputInbox}>
                   <View style={{padding:5,alignItems:'center', justifyContent:'center'}}>
                        <Icon color='black' size={25} name={this.props.icon}/>
                    </View>
                    <View style={{justifyContent:'center', marginRight:10,flex:1}}>  
                        <TextInput secureTextEntry={this.state.isLook} ></TextInput>
                    </View>
                    {this.props.isHide && < ShowPassword/>}
                </View>
            </View>
        
        )
    }
}
export default InputinLine
const styles = StyleSheet.create({
    container:{
 
    },
    inputInbox: {
        width:350,
        backgroundColor:'#C0C0C0',
        borderRadius: 25,
        paddingHorizontal: 15,
        fontSize:15,
        margin:2,
        borderWidth:1,
        flexDirection:'row'
    }
})
