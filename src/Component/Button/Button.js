import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

 class ButtonComp extends Component {
    render() {
        return (
            <TouchableOpacity onPress= {this.props.Click} style= {{backgroundColor:'#191970', padding:10, alignItems:'center', width: 150, borderRadius:20}}>
                    <Text style= {{color:'white',fontWeight:'bold'}}>{this.props.form}</Text>
            </TouchableOpacity>
                          
        )
    }
}
export default ButtonComp
const styles = StyleSheet.create({})
