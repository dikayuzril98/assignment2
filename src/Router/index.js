import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import WelcomeScreen from '../Screens/welcomeScreen'
import LoginScreen from '../Screens/loginScreen'
import RegisterScreen from '../Screens/registerScreen'

const Stack =createStackNavigator ()

const StackNavigator = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name='Welcome' component={WelcomeScreen} options={{headerShown: false}} />
            <Stack.Screen name= 'Login' component={LoginScreen} options={{headerShown: false}}/>
            <Stack.Screen name= 'Register' component={RegisterScreen} options ={{headerShown: false}} />
        </Stack.Navigator>
    )
}

const Router =() => {
    return(
        <NavigationContainer>
            <StackNavigator />
        </NavigationContainer>
    )
}

export default Router