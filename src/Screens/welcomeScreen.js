import React, {Component} from 'react'
import {StyleSheet, View, Text, Image} from 'react-native'

class WelcomeScreen extends Component{
    
    componentDidMount(){

        setTimeout(() => {
            this.props.navigation.replace ('Login')
        }, 2000);
    }
    render(){
        return(
            <View style={style.container}>
                <View style= {{alignItems:'center'}}>
                    <Image source ={require ('../../asseets/images/salt-logo-baru.png')} style= {{height:100, width:100}}/>
                    <Text style ={{fontSize:18, fontWeight:'bold', marginTop:10}}> Salt Academy App</Text>
                </View>
            </View>
        )
    }
}

export default WelcomeScreen

const style = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
    
})