import React ,{Component} from 'react'
import {StyleSheet, View, Text,Image, Button} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import ButtonComp from '../Component/Button/Button';
import InputinLine from '../Component/InputText/InputinLine'

class LoginScreen extends Component{

  toregister = ()=> {
    this.props.navigation.navigate ('Register')
  }
  render(){
    return(
        <View style= {style.container}>
          <View style= {{alignItems:'center'}}>
              <View>
                <Image source ={require ('../../asseets/images/salt-logo-baru.png')} style= {{height:100, width:100}}/>
              </View>
              <Text style={{fontWeight:'bold', fontSize:18,margin:10}}>Salt Academy App</Text>
              <Text style={{fontWeight:'bold', fontSize:13, marginTop:5}}>Please login with a registered account</Text>
          </View>
          <View style={{alignItems:'center'}}>
              <InputinLine icon= 'user' form= 'Username'/>
              <InputinLine icon= 'key' form= 'Password' secure isHide/>
              <ButtonComp form='Login'/>
          </View>
          <View style = {{margin:5}}>
                <View style={{flexDirection:'row'}}>
                     <Text style ={{fontWeight:'bold'}}>Forgot Password</Text>
                     <TouchableOpacity>
                       <Text style={{color: 'red',fontWeight:'bold'}}> Reset Password</Text>
                     </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                     <Text style ={{fontWeight:'bold'}}>Don't have an account </Text>
                     <TouchableOpacity onPress= {this.toregister}>
                       <Text style={{color: 'blue',fontWeight:'bold'}}> Sign up</Text>
                     </TouchableOpacity>
                </View>
          </View>
        </View>

    )
  }
}

export default LoginScreen;

const style= StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    backgroundColor:'#7B68EE'

  }

})